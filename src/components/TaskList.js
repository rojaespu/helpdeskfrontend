import { useEffect, useState } from 'react'
import { Button, Card, CardContent, Typography } from '@mui/material'
import { useNavigate } from 'react-router-dom'

export default function TaskList() {

  const [tasks, setTasks] = useState([]);
  const navigate = useNavigate();

  const loadTasks = async () => {
    const response = await fetch('http://localhost:4000/tasks')
    const data = await response.json()
    setTasks(data)
  }

  const handleDelete = async (id) => {
    
    try {
      const linea = "http://localhost:4000/tasks/" + id

      const res = await fetch(linea, {method: 'DELETE',})
      
      setTasks(tasks.filter(task => task.id !== id));  
      
    } catch (error) {
      console.log(error)
    }
   
  }

  const handleAprobar = async (id) => {
    
    try {
      const linea = "http://localhost:4000/tasksAprobar/" + id

      const res = await fetch(linea, {method: 'PUT',})
      
      loadTasks()
      
    } catch (error) {
      console.log(error)
    }
   
  }


  useEffect(() => {
    loadTasks()
  },[])

  return (
    <>
      <h1>Lista de Solicitudes</h1>
      {
        tasks.map(task => (
          <Card style={{
            marginBottom: '.7rem',
            backgroundColor: '#1e272e'
          }} key={task.id}>
            <CardContent style={{
                display: 'flex',
                justifyContent: 'space-between'
              }}              
              >
              <div style={{color: 'white'}}>
                
                <Typography>Nombre: {task.nombre}</Typography>
                <Typography>Descripcion: {task.descripcion}</Typography>
                <Typography>Prioridad: {task.prioridad}</Typography>
                <Typography>Fecha Creacion: {task.fecha_creacion}</Typography>
                <br></br>
                <Typography>Fecha Aprobacion: {task.fecha_aprobacion}</Typography>
                <Typography>Tecnico Asignado: {task.tecnico}</Typography>
                <Typography>Fecha Asignacion: {task.fecha_asignacion}</Typography>
              </div>
              <div>
                <Button 
                  variant='contained'
                  color='inherit'
                  onClick={() => {
                    const lin = '/tasks/'+task.id+'/edit';
                    navigate(lin);
                  }}
                  >Editar
                </Button>
                <Button 
                  variant='contained'
                  color='warning'
                  onClick={() => {
                    console.log(task.id);
                    handleDelete(task.id);
                  }}
                  style={{marginLeft: '.5rem'}}
                  >Borrar
                </Button>
                <Button 
                  variant='contained'
                  color='secondary'
                  onClick={() => {
                    console.log(task.id);
                    handleAprobar(task.id);
                  }}
                  style={{marginLeft: '.5rem'}}
                  >Aprobar
                </Button>
                {/* onClick={() => navigate('/tasks/new')} */}
                <Button 
                  variant='contained'
                  color='inherit'
                  onClick={() => {
                    const lin = '/tasks/'+task.id+'/asignar';
                    navigate(lin);
                  }}
                  style={{marginLeft: '.5rem'}}
                  >Asignar
                </Button>
              </div>
            </CardContent>
          </Card>
        ))
      }
    </>
  );
}
