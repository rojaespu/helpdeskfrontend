import React from 'react'
import { Button, Card, CardContent, CircularProgress, Grid, TextField, Typography } from '@mui/material' 
import { useState, useEffect} from 'react'
import { useNavigate, useParams } from 'react-router-dom'

export default function TaskForm() {
  const [task, setTask] = useState({
    nombre:'',
    descripcion:'',
    tecnico:''
  })

  const [loading, setLoading] = useState(false)
  const [editing, setEditing] = useState(false)
  const navigate = useNavigate()
  const params = useParams()

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true)

    if (editing) {
      const line = 'http://localhost:4000/tasks/' + params.id
      await fetch(line, {
      method: 'PUT',
      body:JSON.stringify(task),
      headers: { 'Content-Type':'application/json'},
      })
    } else {
      await fetch('http://localhost:4000/tasks', {
      method: 'POST',
      body:JSON.stringify(task),
      headers: { 'Content-Type':'application/json'},
      })
    }
    
    setLoading(false)
    navigate('/')
  }

  const handleChange = e =>{
    setTask({...task, [e.target.name]: e.target.value})
    console.log('rolando taskt====> ' + e.target.name, e.target.value );
  }

  const loadtTask = async (id) => {
    const line = 'http://localhost:4000/tasks/' + id
    const res = await fetch (line)
    const data = await res.json()
    setTask({nombre: data.nombre, descripcion: data.descripcion, prioridad: data.prioridad})
    setEditing(true)
  }

  useEffect(() =>{
    if (params.id){
      loadtTask(params.id)
    }
  }, [params.id])

  return (
    <Grid container direcctions='column' alignItems='center' justifyContent='center'>
      <Grid item xs={3}>
        <Card 
          sx={{mt: 5}}
          style={{
            
            padding:'1rem'
          }}
        >
          <Typography
          variant='5'
          textAlign={'center'}
          color={'white'}
          >
            Create Task
          </Typography>
          <CardContent>
            <form onSubmit={handleSubmit}>
              <TextField 
                variant='filled'
                label='Ingrese su Nombre'
                sx={{
                  display:'block',
                  margin:'.5rem 0'
                }}
                name= 'nombre'
                value={task.nombre}
                onChange={handleChange}
              ></TextField>
              <TextField 
                variant='filled' 
                label='Ingrese su descripcion' 
                multiline rows={4}
                sx={{
                  display:'block',
                  margin:'.5rem 0'
                }}
                name= 'descripcion'
                value={task.descripcion}
                onChange={handleChange}
              ></TextField>
              <TextField 
                variant='filled' 
                label='Ingrese su Prioridad'
                sx={{
                  display:'block',
                  margin:'.5rem 0'
                }}
                name= 'prioridad'
                value={task.prioridad}
                onChange={handleChange}
              ></TextField>
              <br></br>
              <br></br>
              <Button
                variant='contained' 
                color='primary' 
                type='submit'
                disabled = {!task.nombre || !task.descripcion}>
                {loading ? (
                  <CircularProgress color='inherit' size={24}/>
                ) : ("Create")}
              </Button>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}
